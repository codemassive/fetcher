package de.cmassive.vplanx.fetcher.user;

import de.cmassive.vplanx.lib.network.auth.AuthContainer;

public class UserSubscription {

    private int schoolId;
    private int accountId;
    private String featureName;
    private AuthContainer authContainer;

    public UserSubscription(int schoolId, int accountId, String featureName) {
        this.schoolId = schoolId;
        this.accountId = accountId;
        this.featureName = featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public void setAuthContainer(AuthContainer authContainer) {
        this.authContainer = authContainer;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public int getAccountId() {
        return accountId;
    }

    public String getFeatureName() {
        return featureName;
    }

    public AuthContainer getAuthContainer() {
        return authContainer;
    }

    public boolean isCommon() {
        return this.authContainer == null;
    }
}
