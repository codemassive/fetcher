package de.cmassive.vplanx.fetcher.user;

import java.util.ArrayList;
import java.util.HashMap;

public class User {

    private int accountId;
    private HashMap<Integer, ArrayList<UserSubscription>> subscriptionsMap = new HashMap<>();

    public User(int accountId) {
        this.accountId = accountId;
    }

    public int getAccountId() {
        return accountId;
    }

    public HashMap<Integer, ArrayList<UserSubscription>> getSubscriptions() {
        return subscriptionsMap;
    }

    public UserSubscription getSubscription(int schoolId, String ftName) {
        ArrayList<UserSubscription> subscriptions = this.subscriptionsMap.get(schoolId);
        if(subscriptions != null) {
            for(UserSubscription subscription : subscriptions) {
                if(subscription.getFeatureName().equalsIgnoreCase(ftName)) {
                    return subscription;
                }
            }
        }

        return null;
    }

    public ArrayList<UserSubscription> getSubscriptions(int schoolId) {
        return this.subscriptionsMap.getOrDefault(schoolId, new ArrayList<>());
    }

    public void addSubscription(UserSubscription subscription) {
        ArrayList<UserSubscription> subscriptions = this.subscriptionsMap.getOrDefault(subscription.getSchoolId(), new ArrayList<>());
        subscriptions.add(subscription);

        if(!this.subscriptionsMap.containsKey(subscription.getSchoolId())) {
            this.subscriptionsMap.put(subscription.getSchoolId(), subscriptions);
        }
    }
}
