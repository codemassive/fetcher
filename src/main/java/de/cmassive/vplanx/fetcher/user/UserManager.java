package de.cmassive.vplanx.fetcher.user;

import com.mongodb.client.FindIterable;
import de.cmassive.vplanx.fetcher.main.FetcherMain;
import de.cmassive.vplanx.fetcher.session.MongoSession;
import de.cmassive.vplanx.fetcher.task.MongoTaskSource;
import de.cmassive.vplanx.fetcher.util.DocumentUtil;
import de.cmassive.vplanx.lib.session.Session;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import org.bson.Document;

import java.util.*;

public class UserManager {

    private List<User> userList = Collections.synchronizedList(new ArrayList<>()); //TODO: re-think

    public void fetchUsers() {
        FindIterable<Document> documents = FetcherMain.getConnection().getAccountCollection().find();

        for(Document userDocument : documents) {
            int accountId = userDocument.getInteger("accountId");
            User user = new User(accountId);
            updateUser(user, userDocument);
            addUserIfRequired(user);
        }
    }

    public void updateUser(User user, Document userDocument) {
        synchronized (user) { //LOCK USER -> SESSION HAVE TO WAIT FOR UPDATED USER
            user.getSubscriptions().clear();

            int accountId = userDocument.getInteger("accountId");

            List<Document> subscriptionDocs = (List<Document>) userDocument.get("subscriptions");
            for(Document subscriptionDoc : subscriptionDocs) {
                int schoolId = subscriptionDoc.getInteger("schoolId");
                String ftName = subscriptionDoc.getString("feature");

                MongoSession session = FetcherMain.getSessionMgr().getSchoolSession(schoolId);
                if(session != null && !session.isFeatureDynamicAuth(ftName)) {
                    continue;
                }

                UserSubscription subscription = new UserSubscription(schoolId, accountId, ftName);

                if(subscriptionDoc.containsKey("auth")) {
                    subscription.setAuthContainer(DocumentUtil.authInfoFromDocument((Document) subscriptionDoc.get("auth")));
                }

                user.addSubscription(subscription);
            }
        }
    }

    public void updateUser(Document userDocument) {
        int accountId = userDocument.getInteger("accountId");
        User found = getUser(accountId);

        if(found != null) {
            updateUser(found, userDocument);
        } else {
            found = new User(accountId);
            updateUser(found, userDocument);

            if(addUserIfRequired(found)) {
                for(Map.Entry<Integer, ArrayList<UserSubscription>> entry : found.getSubscriptions().entrySet()) { //for each school
                    MongoSession session = FetcherMain.getSessionMgr().getSchoolSession(entry.getKey()); //school id = key

                    for(UserSubscription subscription : entry.getValue()) {
                        synchronized (session) {
                            MongoTaskSource source = (MongoTaskSource) session.getTaskSource();

                            SessionTask task = source.invokeTask(found.getAccountId());
                            if(task != null) {
                                session.execute(task);
                            }

                            break;
                        }
                    }
                }
            }
        }
    }

    private boolean addUserIfRequired(User user) {
        if(user.getSubscriptions().size() > 0) {
            synchronized (userList) {
                userList.add(user);
            }

            return true;
        }

        return false;
    }

    public List<User> getUserList() {
        return userList;
    }

    public User getUser(int userId) {
        for(User user : userList) {
            if(user.getAccountId() == userId) return user;
        }

        return null;
    }
}
