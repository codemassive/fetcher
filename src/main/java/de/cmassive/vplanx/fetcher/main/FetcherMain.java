package de.cmassive.vplanx.fetcher.main;

import com.mongodb.client.model.changestream.ChangeStreamDocument;
import com.mongodb.client.model.changestream.OperationType;
import de.cmassive.vplanx.fetcher.database.MongoConnection;
import de.cmassive.vplanx.fetcher.session.MongoSession;
import de.cmassive.vplanx.fetcher.session.SessionManager;
import de.cmassive.vplanx.fetcher.user.UserManager;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import de.cmassive.vplanx.lib.session.task.SessionTaskExecutor;
import org.bson.Document;

import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.function.Consumer;

public class FetcherMain {

    private static MongoConnection MONGO_CONNECTION;

    private static SessionManager SESSION_MGR;
    private static UserManager USER_MGR;

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static final DateTimeFormatter DATET_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy'T'HH:mm");

    public static MongoConnection getConnection() {
        return MONGO_CONNECTION;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        MONGO_CONNECTION = new MongoConnection(27017);

        SESSION_MGR = new SessionManager();
        USER_MGR = new UserManager();

        SESSION_MGR.fetchSessions();
        USER_MGR.fetchUsers();

        for(MongoSession session : SESSION_MGR.getSessionList()) {
            session.getTaskSource().reload();
        }

        System.out.println(USER_MGR.getUserList().size());

        for(MongoSession session : SESSION_MGR.getSessionList()) {
            SessionRunner.startSession(session);
        }

        Thread userWorker = new Thread(new Runnable() {
            @Override
            public void run() {
                MONGO_CONNECTION.getAccountCollection().watch().forEach(new Consumer<ChangeStreamDocument<Document>>() {
                    @Override
                    public void accept(ChangeStreamDocument<Document> documentChangeStreamDocument) {
                        OperationType type = documentChangeStreamDocument.getOperationType();
                        if(type == OperationType.UPDATE || type == OperationType.INSERT) {
                            USER_MGR.updateUser(documentChangeStreamDocument.getFullDocument());
                        }
                    }
                });
            }
        });

        Document first = MONGO_CONNECTION.getAccountCollection().find().first();

        userWorker.start();
        userWorker.join();
    }

    public static SessionManager getSessionMgr() {
        return SESSION_MGR;
    }

    public static UserManager getUserMgr() {
        return USER_MGR;
    }
}
