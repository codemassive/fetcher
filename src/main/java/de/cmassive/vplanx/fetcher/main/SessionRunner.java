package de.cmassive.vplanx.fetcher.main;

import de.cmassive.vplanx.fetcher.session.MongoSession;
import de.cmassive.vplanx.fetcher.task.MongoTask;
import de.cmassive.vplanx.lib.session.task.SessionTask;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class SessionRunner implements Runnable {

    private MongoSession session;

    private static final Random RANDOM = new Random();
    private static final ScheduledExecutorService SERVICE = Executors.newScheduledThreadPool(5);
    private static final long DELAY = 1000*5;

    public SessionRunner(MongoSession session) {
        this.session = session;
    }

    @Override
    public void run() { //TODO prepare for abstraction layer
        synchronized (session) { //PREVENT SESSION CHANGE ON DB CHANGE
            session.prepareHandlers();

            for(SessionTask task : session.getTaskSource().getTasks()) {
                System.out.println("RUN SESSION TASK WITH ID " + task.getTaskId());
                if(task instanceof MongoTask) {
                    synchronized (((MongoTask) task).getUser()) { //PREVENT USER CHANGE ON DB CHANGE
                        System.out.println("EXECUTE!");
                        session.execute(task);
                    }
                } else {
                    session.execute(task);
                }
            }

            session.releaseHandlers();
        }

        System.out.println("COMPLETE RUN!");
    }

    public static void startSession(MongoSession session) {
        SERVICE.scheduleWithFixedDelay(new SessionRunner(session), 0, DELAY, TimeUnit.MILLISECONDS);
    }
}
