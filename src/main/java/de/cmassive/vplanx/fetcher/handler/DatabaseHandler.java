package de.cmassive.vplanx.fetcher.handler;

import de.cmassive.vplanx.lib.feature.FeatureBase;
import org.bson.Document;

public abstract class DatabaseHandler<T> {

    private int schoolId;

    public DatabaseHandler(Integer schoolId) {
        this.schoolId = schoolId;
    }

    public abstract void read();
    public abstract void write(T feature, int userId);

    public abstract void release();

    public int getSchoolId() {
        return schoolId;
    }
}
