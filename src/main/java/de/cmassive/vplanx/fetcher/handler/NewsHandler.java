package de.cmassive.vplanx.fetcher.handler;

import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.fetcher.main.FetcherMain;
import de.cmassive.vplanx.lib.feature.impl.NewsFeature;
import de.cmassive.vplanx.lib.news.News;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.bson.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NamedClass("ft-news")
public class NewsHandler extends DatabaseHandler<NewsFeature> {

    private Set<News> newsSet = new HashSet<>();
    private Document newsSchoolDoc = null;

    public NewsHandler(Integer schoolId) {
        super(schoolId);
    }

    @Override
    public void read() {
        Document document = FetcherMain.getConnection().getNewsFeatureCollection().find(new BasicDBObject("schoolId", getSchoolId())).first();
        if(document == null) {
            FetcherMain.getConnection().getNewsFeatureCollection().insertOne(new Document("schoolId", getSchoolId()));
            read();
            return;
        }

        newsSchoolDoc = document;

        if(document.containsKey("news")) {
            List<Document> newsDocs = (List<Document>) document.get("news");
            for(Document newsDoc : newsDocs) {
                this.newsSet.add(documentToNews(newsDoc));
            }
        }
    }

    @Override
    public synchronized void write(NewsFeature feature, int userId) { //TODO: News collision check -> replace
        Document document = new Document();
        document.append("schoolId", getSchoolId());
        for(News news : feature.getNewsSet()) {
            news.setId(FetcherMain.getConnection().getNextSequenceId("news"));
            document.append("news", newsListToDocument(feature.getNewsSet()));
        }

        FetcherMain.getConnection().getNewsFeatureCollection().replaceOne(this.newsSchoolDoc, document);
    }

    @Override
    public void release() {
        this.newsSet.clear();
    }

    private static Document newsToDocument(News news) {
        Document document = new Document();
        document.append("newsId", news.getId());
        document.append("headline", news.getHeadline());
        document.append("content", news.getContent());
        document.append("image", news.getMainImageUrl());
        document.append("author", news.getAuthor());
        document.append("published", FetcherMain.DATE_FORMAT.format(news.getPublishedDate()));
        return document;
    }

    private static List<Document> newsListToDocument(Set<News> news) {
        List<Document> document = new ArrayList<>();
        for(News n : news) {
            document.add(newsToDocument(n));
        }
        return document;
    }

    private static News documentToNews(Document document) {
        News news = new News();
        news.setId(document.getInteger("newsId"));
        news.setContent(document.getString("content"));
        news.setHeadline(document.getString("headline"));
        return news;
    }
}
