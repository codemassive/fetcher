package de.cmassive.vplanx.fetcher.handler;


import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;

public class HandlerFactory extends NamedClassFactoryBase<DatabaseHandler> {

    private static HandlerFactory FACTORY = new HandlerFactory();

    protected HandlerFactory() {
        super(DatabaseHandler.class, HandlerFactory.class.getPackage().getName());
    }

    public static HandlerFactory getInstance() {
        return FACTORY;
    }
}
