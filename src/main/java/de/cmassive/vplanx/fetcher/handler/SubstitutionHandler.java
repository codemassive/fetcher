package de.cmassive.vplanx.fetcher.handler;

import com.mongodb.BasicDBObject;
import com.mongodb.client.model.Filters;
import de.cmassive.vplanx.fetcher.main.FetcherMain;
import de.cmassive.vplanx.lib.feature.impl.SubstitutionFeature;
import de.cmassive.vplanx.lib.substitution.Substitution;
import de.cmassive.vplanx.lib.substitution.SubstitutionDay;
import de.cmassive.vplanx.lib.substitution.SubstitutionPlan;
import de.cmassive.vplanx.lib.util.reflection.NamedClass;
import org.bson.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@NamedClass("ft-substitution")
public class SubstitutionHandler extends DatabaseHandler<SubstitutionFeature> {

    private SubstitutionPlan plan = new SubstitutionPlan(); //TODO add support for users
    private HashMap<Integer, ArrayList<SubstitutionDay>> userDays = new HashMap<>();

    public SubstitutionHandler(Integer schoolId) {
        super(schoolId);
    }

    @Override
    public void read() {
        long start = System.currentTimeMillis();

        Document document = FetcherMain.getConnection().getSubstitutionCollection().find(new BasicDBObject("schoolId", getSchoolId())).first();
        if(document == null) {
            throw new RuntimeException("HND> Document for school id " + getSchoolId() + " not found (ft-substitution)");
        }

        List<Document> dayDocs = (List<Document>) document.get("days");
        if(document.containsKey("userDays")) {
            dayDocs.addAll((Collection<? extends Document>) document.get("userDays"));
        }

        for(Document dayDoc : dayDocs) {
            LocalDate date = LocalDate.parse(dayDoc.getString("date"), FetcherMain.DATE_FORMAT);

            if(date.isBefore(LocalDate.now())) { //delete day
                FetcherMain.getConnection().getSubstitutionCollection().updateOne(
                        Filters.eq("schoolId", getSchoolId()),
                        new BasicDBObject("$pull", new BasicDBObject(dayDoc.containsKey("accountId") ? "userDays" : "days", new BasicDBObject("dayId", dayDoc.getInteger("dayId")))));
                continue;
            }

            SubstitutionDay day = new SubstitutionDay(date);
            day.setId(dayDoc.getInteger("dayId"));

            List<Document> subDocs = (List<Document>) dayDoc.get("subs");
            for(Document subDoc : subDocs) {
                day.addSubstitution(documentToSub(subDoc));
            }

            day.setMessageOfTheDay(dayDoc.getString("motd"));

            if(document.containsKey("accountId")) {
                ArrayList<SubstitutionDay> userDayList = this.userDays.getOrDefault(document.getInteger("accountId"), new ArrayList<>());
                userDayList.add(day);
                userDays.put(document.getInteger("accountId"), userDayList);
            } else {
                this.plan.addDay(day);
            }
        }

        System.out.println("READ TOOK " + (System.currentTimeMillis()-start));
    }

    @Override
    public synchronized void write(SubstitutionFeature ft, int userId) {
        writeClasses(this.getSchoolId(), ft.getClasses());

        if(userId == -1) {
            collectionWrite(ft.getPlan().getDays(), this.plan.getDays(), userId);
        } else {
            ArrayList<SubstitutionDay> arrayList = this.userDays.getOrDefault(userId, new ArrayList<>());
            collectionWrite(ft.getPlan().getDays(), arrayList, userId);

            this.userDays.put(userId, arrayList);
        }
    }

    @Override
    public void release() {
        this.plan.getDays().clear();
        this.userDays.clear();
    }


    private static Set<String> listToSet(List<String> str) {
        if(str == null) return new HashSet<>();

        HashSet<String> set = new HashSet<>();
        set.addAll(str);

        return set;
    }

    private static SubstitutionDay getDayFromCollection(LocalDate date, Collection<SubstitutionDay> collection) {
        for(SubstitutionDay day : collection) {
            if(day.getDate().equals(date)) return day;
        }

        return null;
    }

    private static void replaceDayOfCollection(SubstitutionDay day, Collection<SubstitutionDay> collection) {
        collection.removeIf((d) -> {
            return d.getDate().equals(day.getDate());
        });

        collection.add(day);
    }

    private void collectionWrite(Collection<SubstitutionDay> source, Collection<SubstitutionDay> target, int userId) {
        for(SubstitutionDay day : source) {
            SubstitutionDay search = null;
            if((search = getDayFromCollection(day.getDate(), target)) != null) {
                day.setId(search.getId());

                outer: for(Substitution newSubstitution : day.getSubstitutionList()) {
                    inner: for(Substitution oldSubstitution : search.getSubstitutionList()) {
                        if((
                                Objects.equals(newSubstitution.getDateTime(), oldSubstitution.getDateTime()) &&
                                Objects.equals(newSubstitution.getOldRoom(), oldSubstitution.getOldRoom()) &&
                                Objects.equals(newSubstitution.getOldSubject(), oldSubstitution.getOldSubject()) &&
                                Objects.equals(newSubstitution.getOldTeacher(), oldSubstitution.getOldTeacher()))) {
                            newSubstitution.setId(oldSubstitution.getId());
                            continue outer;
                        }

                        newSubstitution.setId(FetcherMain.getConnection().getNextSequenceId("subs"));
                    }
                }

                replaceDayOfCollection(day, target);
                //plan.setDay(day.getDate(), day);

                writeDay(day, getSchoolId(), userId);

                System.out.println("MGR EX DAY!");
                continue;
            }

            System.out.println("NEW DAY!");
            for(Substitution substitution : day.getSubstitutionList()) {
                substitution.setId(FetcherMain.getConnection().getNextSequenceId("subs"));
            }
            day.setId(FetcherMain.getConnection().getNextSequenceId("days"));

            target.add(day);
            writeDay(day, getSchoolId(), userId);
        }

    }


    private static Substitution documentToSub(Document subDoc) {
        LocalDateTime subDate = LocalDateTime.parse(subDoc.getString("date"), FetcherMain.DATET_FORMAT);

        Substitution sub = new Substitution();
        sub.setId(subDoc.getInteger("subId"));
        sub.setDateTime(subDate);

        sub.setNewSubject(subDoc.getString("newSubject"));
        sub.setOldSubject(subDoc.getString("oldSubject"));
        sub.setNewRoom(subDoc.getString("newRoom"));
        sub.setOldRoom(subDoc.getString("oldRoom"));
        sub.setDescription(subDoc.getString("description"));
        sub.setOldTeacher(listToSet((List<String>) subDoc.get("oldTeacher")));
        sub.setNewTeacher(listToSet((List<String>) subDoc.get("newTeacher")));
        sub.setLessonString(subDoc.getString("lessonName"));
        sub.setClasses(listToSet((List<String>) subDoc.get("classes")));

        return sub;
    }

    private static Document dayToDocument(SubstitutionDay day, int userId) {
        Document dayDoc = new Document();
        dayDoc.append("date", FetcherMain.DATE_FORMAT.format(day.getDate()));
        dayDoc.append("dayId", day.getId());

        if(userId != -1) {
            dayDoc.append("accountId", userId);
        }

        List<Document> subs = new ArrayList<>();
        for(Substitution sub : day.getSubstitutionList()) {
            Document doc = new Document();

            doc.append("date", FetcherMain.DATET_FORMAT.format(sub.getDateTime()));
            doc.append("newSubject", sub.getNewSubject());
            doc.append("oldSubject", sub.getOldSubject());
            doc.append("newRoom", sub.getNewRoom());
            doc.append("oldRoom", sub.getOldRoom());
            doc.append("newTeacher", sub.getNewTeacher());
            doc.append("oldTeacher", sub.getOldTeacher());
            doc.append("description", sub.getDescription());
            doc.append("lessonName", sub.getLessonString());
            doc.append("subId", sub.getId());
            doc.append("classes", sub.getClasses());

            subs.add(doc);
        }

        dayDoc.append("motd", day.getMessageOfTheDay());
        dayDoc.append("subs", subs);

        return dayDoc;
    }

    private static void writeDay(SubstitutionDay day, int schoolId, int accountId) {
        //System.out.println("DAY JSON: " + dayToDocument(day).toJson());
        Document query = null;

        if(accountId == -1) {
            query = new Document().append("schoolId", schoolId).append("days.dayId", day.getId());
        } else {
            query = new Document().append("schoolId", schoolId).append("userDays.dayId", day.getId());
        }

        if(FetcherMain.getConnection().getSubstitutionCollection().find(query).first() == null) {
            BasicDBObject arrayQuery = new BasicDBObject((accountId == -1 ? "days" : "userDays"), dayToDocument(day, accountId));
            BasicDBObject set = new BasicDBObject("$push", arrayQuery);
            FetcherMain.getConnection().getSubstitutionCollection().updateOne(new BasicDBObject("schoolId", schoolId), set);
        } else {
            BasicDBObject arrayQuery = new BasicDBObject((accountId == -1 ? "days" : "userDays") + ".$", dayToDocument(day, accountId));
            BasicDBObject set = new BasicDBObject("$set", arrayQuery);
            FetcherMain.getConnection().getSubstitutionCollection().updateOne(query, set);
        }

    }

    private static void writeClasses(int schoolId, Set<String> classes) {
        FetcherMain.getConnection().getSubstitutionCollection().updateOne(
                new BasicDBObject("schoolId", schoolId),
                new BasicDBObject("$set", new BasicDBObject("classes", classes)));
    }
}
