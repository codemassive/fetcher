package de.cmassive.vplanx.fetcher.session;

public class SessionFlag {

    public static final String FLAG_AUTH_SOURCE = "authDataSource";

    private String name;
    private String value;

    public SessionFlag(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
