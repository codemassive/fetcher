package de.cmassive.vplanx.fetcher.session;

import de.cmassive.vplanx.fetcher.handler.DatabaseHandler;
import de.cmassive.vplanx.lib.session.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MongoSession extends Session {

    private HashMap<String, DatabaseHandler> handlers = new HashMap<>();
    private HashMap<String, ArrayList<SessionFlag>> flags = new HashMap<>();

    public HashMap<String, DatabaseHandler> getHandlers() {
        return handlers;
    }
    public void setHandlers(HashMap<String, DatabaseHandler> handlers) {
        this.handlers = handlers;
    }

    public HashMap<String, ArrayList<SessionFlag>> getFlags() {
        return flags;
    }
    public void setFlags(HashMap<String, ArrayList<SessionFlag>> flags) {
        this.flags = flags;
    }
    public boolean hasFlag(String ftName, String name) {
        for(SessionFlag flag : flags.get(ftName)) {
            if(flag.getName().equalsIgnoreCase(name)) return true;
        }
        return false;
    }
    public SessionFlag getFlag(String ftName, String name) {
        for(SessionFlag flag : flags.getOrDefault(ftName, new ArrayList<>())) {
            if(flag.getName().equalsIgnoreCase(name)) return flag;
        }
        return null;
    }

    public void addFlag(String ftName, SessionFlag flag) {
        ArrayList<SessionFlag> flags = this.flags.getOrDefault(ftName, new ArrayList<>());
        flags.add(flag);
        this.flags.put(ftName, flags);
    }

    public boolean isFeatureDynamicAuth(String ftName) {
        SessionFlag flag = getFlag(ftName, SessionFlag.FLAG_AUTH_SOURCE);
        return flag != null && flag.getValue().equalsIgnoreCase("dynamic");
    }

    public void releaseHandlers() {
        for(DatabaseHandler handler : this.handlers.values()) {
            handler.release();
        }
    }

    public void prepareHandlers() {
        for(DatabaseHandler handler : this.handlers.values()) {
            handler.read();
        }
    }
}
