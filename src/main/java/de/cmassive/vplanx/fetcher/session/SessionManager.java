package de.cmassive.vplanx.fetcher.session;

import com.mongodb.client.FindIterable;
import de.cmassive.vplanx.fetcher.handler.DatabaseHandler;
import de.cmassive.vplanx.fetcher.handler.HandlerFactory;
import de.cmassive.vplanx.fetcher.main.FetcherMain;
import de.cmassive.vplanx.fetcher.task.MongoTaskSource;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.feature.impl.NewsFeature;
import de.cmassive.vplanx.lib.feature.impl.SubstitutionFeature;
import de.cmassive.vplanx.lib.network.auth.Endpoint;
import de.cmassive.vplanx.lib.util.reflection.NamedClassFactoryBase;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;
import org.bson.Document;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SessionManager { //TODO: implement session change (f.e url/auth-data of school changes at runtime in DB)

    private ArrayList<MongoSession> sessionList = new ArrayList<>();


    public void fetchSessions() {
        FindIterable<Document> documents = FetcherMain.getConnection().getSchoolFeatureCollection().find();
        for(Document doc : documents) {
            int schoolId = doc.getInteger("schoolId");

            List<Document> featureDocs = (List<Document>) doc.get("features");

            MongoSession session = new MongoSession();
            session.setTaskSource(new MongoTaskSource(schoolId));
            session.setId(schoolId);

            for(Document ftDoc : featureDocs) {
                FeatureBase base = constructBase(ftDoc);
                if(base != null) {
                    System.out.println("ADD " + ReflectionUtil.getNamedClassName(base.getClass()));
                    session.addFeature(base);

                    DatabaseHandler handler = HandlerFactory.getInstance().newInstance(ftDoc.getString("name"), schoolId);
                    if(handler != null) {
                        session.getHandlers().put(ftDoc.getString("name"), handler);
                    }

                    if(ftDoc.containsKey("flags")) {
                        List<Document> flagDocs = (List<Document>) ftDoc.get("flags");
                        for(Document flagDoc : flagDocs) {
                            session.addFlag(ftDoc.getString("name"), new SessionFlag(flagDoc.getString("name"), flagDoc.getString("value")));
                        }
                    }
                }
            }

            sessionList.add(session);
            //session.getTaskSource().reload();
        }
    }

    public synchronized void refreshSessions() {
        sessionList.clear();
        fetchSessions();
    }

    public List<MongoSession> getSessionList() {
        return sessionList;
    }

    public MongoSession getSchoolSession(int schoolId) {
        for(MongoSession info : sessionList) {
            if(info.getId() == schoolId) {
                return info;
            }
        }

        return null;
    }

    private static FeatureBase constructBase(Document document) {
        FeatureBase base = null;
        String name = document.getString("name");

        if(name.equalsIgnoreCase("ft-substitution")) {
            base = new SubstitutionFeature(((Document)document.get("data")).getString("api"));
        } else if(name.equalsIgnoreCase("ft-news")) {
            base = new NewsFeature(((Document)document.get("data")).getString("loader"));
        }

        if(base != null) {
            List<String> urls = (List<String>) document.get("urls");
            for(String url : urls) {
                try {
                    base.getEndpoints().add(new Endpoint(new URL(url)));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        return base;
    }

}
