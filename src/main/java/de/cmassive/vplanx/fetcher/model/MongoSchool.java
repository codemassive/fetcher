package de.cmassive.vplanx.fetcher.model;

import de.cmassive.vplanx.lib.substitution.SubstitutionDay;
import de.cmassive.vplanx.lib.substitution.SubstitutionPlan;

import java.time.LocalDate;

public class MongoSchool {

    private int schoolId;
    private SubstitutionPlan substitutionPlan;

    public MongoSchool(int id) {
        this.schoolId = id;
    }

    public SubstitutionPlan getSubstitutionPlan() {
        return substitutionPlan;
    }
    public void setSubstitutionPlan(SubstitutionPlan substitutionPlan) {
        this.substitutionPlan = substitutionPlan;
    }
}
