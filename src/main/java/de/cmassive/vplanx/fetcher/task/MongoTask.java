package de.cmassive.vplanx.fetcher.task;

import de.cmassive.vplanx.fetcher.user.User;
import de.cmassive.vplanx.fetcher.user.UserSubscription;
import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.data.FeatureDataFactory;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import de.cmassive.vplanx.lib.session.task.TaskDataBucket;

import java.util.ArrayList;
import java.util.List;

public class MongoTask extends SessionTask { //TODO: merge with functionality of BasicSessionTask

    private User user;
    private int schoolId;

    public MongoTask(User user, int taskSchoolId) {
        this.user = user;
        this.schoolId = taskSchoolId;
    }

    @Override
    public TaskDataBucket getDataBucket(String s) {
        UserSubscription subscription = this.user.getSubscription(this.schoolId, s);
        if(subscription != null) {
            FeatureDataBase base = FeatureDataFactory.getInstance().newInstance(s);
            base.setAuthContainer(subscription.getAuthContainer());
            return new TaskDataBucket(s, base);
        }
        return null;
    }

    @Override
    public boolean hasBucket(String s) {
        return this.user.getSubscription(this.schoolId, s) != null;
    }

    @Override
    public List<TaskDataBucket> getDataBuckets() {
        return new ArrayList<>();
    }

    public User getUser() {
        return user;
    }

    public int getSchoolId() {
        return schoolId;
    }
}
