package de.cmassive.vplanx.fetcher.task;

import com.mongodb.BasicDBObject;
import de.cmassive.vplanx.fetcher.main.FetcherMain;
import de.cmassive.vplanx.fetcher.session.MongoSession;
import de.cmassive.vplanx.fetcher.session.SessionFlag;
import de.cmassive.vplanx.fetcher.user.User;
import de.cmassive.vplanx.fetcher.user.UserSubscription;
import de.cmassive.vplanx.fetcher.util.DocumentUtil;
import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.data.FeatureDataFactory;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.Endpoint;
import de.cmassive.vplanx.lib.session.Session;
import de.cmassive.vplanx.lib.session.task.BasicSessionTask;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import de.cmassive.vplanx.lib.session.task.TaskDataBucket;
import org.bson.Document;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MongoTaskSource implements de.cmassive.vplanx.lib.session.SessionTaskSource {

    private static DbTaskObserver taskObserver = new DbTaskObserver();

    private List<SessionTask> tasks = new ArrayList<>();
    private int schoolId;

    public MongoTaskSource(int schoolId) {
        this.schoolId = schoolId;
    }

    @Override
    public List<SessionTask> getTasks() {
        return this.tasks;
    }


    @Override
    public boolean reload(int taskId) {
        return true; //No reload needed -> task holds reference to user object
    }


    @Override
    public boolean reload() {
        try {
            tasks.clear();
            return fetchTasks();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public SessionTask invokeTask(int userId) {
        for(SessionTask task : this.tasks) {
            if(task instanceof MongoTask) {
                MongoTask mongoTask = (MongoTask) task;
                if(mongoTask.getUser().getAccountId() == userId) {
                    return null;
                }
            }
        }

        SessionTask task = createUserTask(FetcherMain.getUserMgr().getUser(userId), this.schoolId);
        this.tasks.add(task);
        return task;
    }

    private boolean fetchTasks() throws MalformedURLException {
        BasicDBObject query = new BasicDBObject("schoolId", this.schoolId);
        Document document = FetcherMain.getConnection().getSchoolFeatureCollection().find(query).first();
        if(document == null) return false;

        boolean needUserFetch = false;

        if(document.containsKey("features")) {
            BasicSessionTask rootTask = new BasicSessionTask();
            List<Document> documentListFt = (List<Document>) document.get("features");

            for(Document documentFt : documentListFt) {
                String ftName = documentFt.getString("name");
                FeatureDataBase dataBase = FeatureDataFactory.getInstance().newInstance(ftName);
                if(dataBase == null) continue;

                SessionFlag authFlag = FetcherMain.getSessionMgr().getSchoolSession(this.schoolId).getFlag(ftName, SessionFlag.FLAG_AUTH_SOURCE);
                if(authFlag == null || !authFlag.getValue().equalsIgnoreCase("dynamic")) {
                    if(documentFt.containsKey("auth")) {
                        Document authDoc = (Document) documentFt.get("auth");
                        AuthContainer container = DocumentUtil.authInfoFromDocument(authDoc);
                        dataBase.setAuthContainer(container);
                    }
                } else {
                    needUserFetch = true;
                    continue;
                }

                rootTask.addData(new TaskDataBucket(ftName, dataBase));
            }

            if(rootTask.getDataBuckets().size() != 0) {
                rootTask.setTaskId(-1);
                rootTask.setObserver(taskObserver);
                this.tasks.add(rootTask);
            }
        }

        if(needUserFetch) return fetchUserTasks();
        return true;
    }

    private boolean fetchUserTasks() {
        MongoSession session = FetcherMain.getSessionMgr().getSchoolSession(this.schoolId);

        for(User user : FetcherMain.getUserMgr().getUserList()) {
            if(user.getSubscriptions(this.schoolId).size() > 0) {
                this.tasks.add(createUserTask(user, this.schoolId));
            }
        }

        return true;
    }

    private MongoTask createUserTask(User user, int schoolId) {
        MongoTask task = new MongoTask(user, schoolId);
        task.setObserver(taskObserver);
        task.setTaskId(user.getAccountId());
        return task;
    }
}
