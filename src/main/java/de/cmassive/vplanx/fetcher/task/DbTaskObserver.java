package de.cmassive.vplanx.fetcher.task;

import de.cmassive.vplanx.fetcher.handler.DatabaseHandler;
import de.cmassive.vplanx.fetcher.session.MongoSession;
import de.cmassive.vplanx.lib.data.FeatureDataBase;
import de.cmassive.vplanx.lib.feature.FeatureBase;
import de.cmassive.vplanx.lib.session.Session;
import de.cmassive.vplanx.lib.session.task.SessionTask;
import de.cmassive.vplanx.lib.util.reflection.ReflectionUtil;

import java.util.HashMap;

public class DbTaskObserver implements SessionTask.Observer {


    @Override
    public void onTaskStart(SessionTask sessionTask, Session session) {

    }

    @Override
    public synchronized boolean onTaskProcess(SessionTask sessionTask, Session session, FeatureBase featureBase, FeatureDataBase featureDataBase) {
        if(session instanceof MongoSession) {
            MongoSession mongoSession = (MongoSession) session;
            DatabaseHandler handler = mongoSession.getHandlers().get(ReflectionUtil.getNamedClassName(featureBase.getClass()));

            if(handler != null) {
                handler.write(featureBase, sessionTask.getTaskId());
            }
        }

        return true;
    }

    @Override
    public boolean onTaskError(SessionTask sessionTask, Session session, FeatureBase featureBase, FeatureDataBase featureDataBase, Exception e) {
        e.printStackTrace();
        return true;
    }

    @Override
    public void onTaskFinish(SessionTask sessionTask, Session session) {

    }
}
