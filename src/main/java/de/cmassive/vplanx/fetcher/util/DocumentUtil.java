package de.cmassive.vplanx.fetcher.util;

import de.cmassive.vplanx.lib.network.auth.AuthContainer;
import de.cmassive.vplanx.lib.network.auth.info.AuthInfo;
import de.cmassive.vplanx.lib.network.auth.info.UserPasswordAuth;
import org.bson.Document;

public class DocumentUtil {

    public static AuthContainer authInfoFromDocument(Document doc) {
        AuthContainer info = new AuthContainer();
        info.setExpectedResult(doc.getString("expected"));
        info.setAuthType(doc.getString("type"));

        Document data = (Document) doc.get("data");

        switch(doc.getString("type")) { //TODO impl other types
            case "auth-static" :
                info.setAuthInfo(new UserPasswordAuth(data.getString("username"), data.getString("password")));
                break;
        }

        return info;
    }

}
